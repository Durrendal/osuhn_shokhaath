# What?

Osuhn shokha'ath (Alt Redoranis) - We had written

Scrape the contents of each .adventure_log file in all users directories and display the resulting updates from today and yesterday

![Today's Updates](.img/osa-today.png)

![Yesterday's Updates](.img/osa-yesterday.png)

## Weird name?

I've been playing a lot of Morrowind lately, it's Dunmeri. Hopefully this nerd snipes you and you too want to revisit the best Elder Scrolls game now.

## Build

```
make prep
make compile
make install
```

## Usage:

So long as there's users in /home who have .adventure_log files in their home directory you should see their last two updates relative to the server local time.

the command to invoke the tui is:
```
osa
```

Once inside the tui the following keys are bound:
```
T - Today's Updates
Y - Yesterday's Updates
U - Specific User's Updates
ESC or Q - quit
```

Adventure logs can be of the following formats
```
01 DEC: some update text
02 Dec: another entry
03 dec: yet another entry

04 DEC: we can break entries with new lines
05 DEC: if our update text exceeds the width of the buffer it'll be truncated (as shown in the yesterday image up top!)
```

The tui launches in full screen mode, but doesn't attempt to wrap text in any way, it currently truncates it if it's too long.

## Fetch Utility

If you want to pull in .adventure_log files from external sources (ie: blogs or git), you can configure the fetch_file utility to pull these per user.
```
make fetch-util
make install-util
```

Created /etc/av_fetch.yaml with the following values per user
```
Users:
  - Name: wsinatra
    Url: https://lambdacreate.com/adventure_log
    Parse: false #file is provided in "01 DEC: update" format already
  - Name: someone
    Url: https://somewhere/else/updates.html
    Parse: true  #Attempt to parse html into "01 DEC: update" format
```
The user's name should make the username on the *nix instance the utility runs

Then configure crontab to run the utility every hour, or more/less as desired
```
* 1 * * * /usr/local/bin/osa_fetch
```