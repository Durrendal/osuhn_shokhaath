OUTDIR ?= ~/.local/bin/

prep:
	nimble install illwill

compile:
	cd src && nim c -d:release osa.nim
	cd src && nim c -d:release twtxt_export.nim
	cd src && nim c -d:ssl -d:release osa_fetch.nim

install:
	mv src/osa $(OUTDIR)
	mv src/osa_fetch $(OUTDIR)
	mv src/twtxt_export $(OUTDIR)
