import std/[os, strutils, times, sequtils]

type
  Log* = tuple
    date: string
    update: string
    owner: string

type
  User* = tuple
    name: string
    home: string
    log: string

#Truncate strings to a max len
proc toString*(str: seq[char], maxlen: int): string =
  var
    counter = 1
    ret = newStringOfCap(maxlen-2)
  for ch in str:
    if counter != maxlen:
      ret = ret & ch
      inc(counter)
    else:
      return ret & ".."
  return ret      

#Whoever is on the server and has a .adventure_log
proc gatherUsers*(): seq[User] =
  var
    users = newSeq[User]()

  for dir in walkDir("/home"):
    if fileExists(dir.path & "/.adventure_log"):
      let
        split = dir.path.split("/")
        user: User = (name: split[2],
                      home: dir.path,
                      log: dir.path & "/.adventure_log")
      users.add(user)
  return users

#Parse 01 DEC: update into a sequence of tuples
proc parseLogs*(users: seq[User]): seq[Log] =
  var
    logs = newSeq[Log]()

  for user in users:
    for line in lines user.log:
      if line.len != 0:
        let
          split = line.split(':')
          log: Log = (date: toUpper(split[0]),
                      update: split[1],
                      owner: user.name)
        logs.add(log)
  return(logs)

#Matches 01 JAN: through 31 DEC:
proc getAdventures*(lag: int): seq[Log] =
  var
    adventureLogs = newSeq[Log]()
  let
    users = gatherUsers()
    logs = parseLogs(users)
    d = now() - lag.days
    dayof = toUpper(d.format("dd MMM"))

  for log in logs:
    if log.date.contains(dayof):
      adventureLogs.add(log)
  return adventureLogs

proc getUserAdventures*(user: string): seq[Log] =
  var
    userLogs = newSeq[Log]()
  let
    adventureLog = "/home/" & user & "/.adventure_log"

  if fileExists(adventureLog):
    for line in lines adventureLog:
      if line.len != 0:
        let
          split = line.split(':')
          log: Log = (date: toUpper(split[0]),
                      update: split[1],
                      owner: user)
        userLogs.add(log)
  return userLogs

