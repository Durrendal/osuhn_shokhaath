import std/[os, strutils, sequtils, wordwrap], illwill
import data
#nim c -d:release osa.nim

#Exit the tui
proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)

proc drawHeader(tbuf: var TerminalBuffer) =
  tbuf.clear()
  tbuf.drawRect(0, 0, tbuf.width-1, tbuf.height-1)
  tbuf.write(2, 1, fgRed, "Osuhn Shokha\'ath")
  tbuf.setForegroundColor(fgWhite, true)
  tbuf.drawHorizLine(2, tbuf.width-3, 2, true)

proc drawUsage(tbuf: var TerminalBuffer) =
  tbuf.setForegroundColor(fgWhite, true)
  tbuf.drawHorizLine(2, tbuf.width-3, tbuf.height-5, true)
  tbuf.write(2, tbuf.height-3, fgGreen, "T", fgWhite, " - Today's updates ", fgGreen, "Y", fgWhite, " - Yesterday's Updates ", fgGreen, "U", fgWhite, " - Updates for a Specific User ", fgGreen, "ESC or Q", fgWhite, " - Quit")
  tbuf.display()

proc drawToday(tbuf: var TerminalBuffer, x: int, y: int) =
  tbuf.write(2, 4, fgYellow, "Today's Adventures")
  var
    y = 4
    idx = 1
  let
    today = getAdventures(0)
  for log in today:
    if y + idx <= tbuf.height-5:
      tbuf.write(2, y + idx, resetStyle, intToStr(idx) & ") ", fgBlue, log.owner, fgYellow, " [" & log.date & "]")
      let
        logstr = wrapWords(log.update, tbuf.width-8).split("\n")
      for line in logstr:
        tbuf.write(2, y + idx + 1, fgWhite, line)
        inc(y)
      inc(idx)

proc drawYesterday(tbuf: var TerminalBuffer, x: var int, y: var int) =
    tbuf.write(2, 4, fgYellow, "Yesterday's Adventures")
    var
      y = 4
      idx = 1
    let
      yesterday = getAdventures(1)
    for log in yesterday:
      if y + idx <= tbuf.height-5:
        tbuf.write(2, y + idx, resetStyle, intToStr(idx) & ") ", fgBlue, log.owner, fgYellow, " [" & log.date & "]")
      let
        logstr = wrapWords(log.update, tbuf.width-8).split("\n")
      for line in logstr:
        tbuf.write(2, y + idx + 1, fgWhite, line)
        inc(y)
      inc(idx)

#This isn't exactly right, it renders initially, trying to "scroll" blanks the page. I think it's because we're over incremeneting idx/cy
#This is all very clunky, and there's probably way better ways to deal with it.
proc drawUser(tbuf: var TerminalBuffer) =
  var
    inview = true
    cy = 4
    idx = 1
    ure = getUserAdventures("durrendal")

  drawHeader(tbuf)
  tbuf.write(2, 4, fgYellow, "User Adventures!")
  for i, log in ure:
    if i >= idx:
      if cy + idx <= tbuf.height-10:
        tbuf.write(2, cy + idx, resetStyle, $idx & ") ", fgBlue, log.owner, fgYellow, " [" & log.date & "]")
      let
        logstr = wrapWords(log.update, tbuf.width-8).split("\n")
      for line in logstr:
        tbuf.write(2, cy + idx, fgWhite, line)
        inc(cy)
      inc(idx)
  drawUsage(tbuf)
    
  while inview == true:
    var
      key = getKey()
    case key
    of Key.None:
      discard
    of Key.Up:
      dec(cy)
      discard
    of Key.Down:
      inc(cy)
      drawHeader(tbuf)
      tbuf.write(2, 4, fgYellow, "User Adventures!")
      for i, log in ure:
        if i >= idx:
          if cy <= tbuf.height-10:
            tbuf.write(2, cy + idx, resetStyle, $idx & ") ", fgBlue, log.owner, fgYellow, " [" & log.date & "]")
          let
            logstr = wrapWords(log.update, tbuf.width-8).split("\n")
          for line in logstr:
            tbuf.write(2, cy + idx, fgWhite, line)
            inc(cy)
          inc(idx)
      drawUsage(tbuf)
    else:
      inview = false
      drawHeader(tbuf)
      drawUsage(tbuf)

proc main(): void =
  illwillInit(fullscreen=true)
  setControlCHook(exitProc)
  hideCursor()

  var
    tbuf = newTerminalBuffer(terminalWidth(), terminalHeight())
    x = 0
    y = 0

  drawHeader(tbuf)
  drawToday(tbuf, x, y)
  drawUsage(tbuf)

  while true:
    var
      key = getKey()
    case key
    of Key.None:
      discard
    of Key.Escape, Key.Q:
      exitProc()
    of Key.T:
      drawHeader(tbuf)
      drawToday(tbuf, x, y)
      drawUsage(tbuf)
    of Key.Y:
      drawHeader(tbuf)
      drawYesterday(tbuf, x, y)
      drawUsage(tbuf)
    of Key.U:
      drawUser(tbuf)
    else:
      discard
      
    sleep(20)

main()
