import std/[os, httpclient, strutils, streams, htmlparser, xmltree], yaml

type User = tuple
    Name: string
    Url: string
    Parse: bool

type Conf = object
    Users: seq[User]

var config: Conf

# av_fetch.yaml should look like this:
#Users:
#  - Name: wsinatra
#    Url: https://lambdacreate.com/adventure_log
#    Parse: false #file is provided in "01 DEC: update" format already
#  - Name: someone
#    Url: https://somewhere/else/updates.html
#    Parse: true  #Attempt to parse html into "01 DEC: update" format


# Pull a file via http, output to /home/$user/.adventure_log
proc fetch(item: string, url: string) =
  var
    client = newHttpClient()

  try:
    var file = open("/home/" & item & "/.adventure_log", fmwrite)
    defer: file.close()
    file.write(client.getContent(url))
  except IOError as err:
    echo("Failed to download " & $item & ": " & err.msg)

# Pull a file, attempt to parse out <p><code></code></p> blocks with "01 DEC: update" entries.
# Very WIP, doesn't work as expected yet
proc fetchParse(item: string, url: string) =
  var
    client = newHttpClient()

  try:
    var file = open("/tmp/" & $item, fmwrite)
    defer: file.close()
    file.write(client.getContent(url))
  except IOError as err:
    echo("Failed to download " & $item & ": " & err.msg)

  let
    html = loadHtml("/tmp/" & item)

  var file = open("/home/" & item & "/.adventure_log", fmwrite)
  defer: file.close()
  for attr in html.findAll("p"):
    file.write(attr)
    file.write("\n")

# Read in yaml config file
proc configure(): void =
  var f = newFileStream("/etc/osa_fetch.yaml")
  load(f, config)
  f.close()

# Configure, then fetch
configure()

for user in config.Users:
  if user.Parse:
    fetchParse(user.Name, user.Url)
  else:
    fetch(user.Name, user.Url)
