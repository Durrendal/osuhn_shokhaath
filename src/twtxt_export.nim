import std/[os, strutils, sequtils, times]
import data
#nim c twtxt_export

proc twtxtName(name: string): string =
  return "\t@<" & name & " https://smallandnearlysilent.com/advent_of_ure.txt> "

proc twtxtDate(date: string, year: string): string =
  let
    #this kludge only exists because we don't include years in the spec
    dt = parse(date & " " & year, "dd MMM yyyy")
    dtf = dt.format("yyyy-MM-dd")
  return dtf & "T00:00:00+00:00"

proc logToTwtxt(file: string): void =
  var
    users = gatherUsers()
    logs = parseLogs(users)
    f = open(file, fmwrite)
  defer: f.close()
  for log in logs:
    f.write(twtxtDate(log.date, "2022") & twtxtName(log.owner) & log.update & "\n")

logToTwtxt("advent_of_ure.txt")
